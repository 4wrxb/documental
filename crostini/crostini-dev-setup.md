# Crostini Dev Setup

*Migrated from Gist Dec 2019, see [Key Resources](#key-resources) for origins*

General caution: Chrome OS is a secure OS by design, but this has at least one key consequence. If you change your Google account password, you will still be required to enter the old password he next time you access each Chrome OS device. Devices are encrypted with that password, so the OS needs to decrypt using the old password then re-encrypt using the new one. If you forget your old password you will lose access to your Chrome OS device data. As always, make sure you keep backups up to date.

## Change your channel

~~Some of the features mentioned in this document only work on the beta or Dev channel~~ as of end 2019 stable channel seems to have all below features. To change your channel:

1. chrome://help in a browser window
1. Click Detailed Build Information
1. Change Channel
1. Select Beta (Or Dev, if you're feeling adventurous. This [guide to choosing](https://www.techrepublic.com/article/which-chrome-os-channel-is-right-for-you/) is useful).
1. Wait for Chromebook to download an update then show icon in bottom left
1. Restart when update is ready

## Fast User Switching

If you have multiple accounts (Say, work and play), you can quickly sitch between them without logging out:

1. Open the status area (the bottom-right menu) 
1. Click your account photo.
1. Click Sign in another user.
1. You'll see a reminder to use the feature only with people you trust. Click OK.
1. Pick a person, then enter their password. You can add up to 7 users.

You can then quick-switch between users with CTRL+ALT+, & CTRL+ALT+.


## Enable Power Button as 2FA (Pixelbook/Pixel Slate/etc.)

Pixelbook's power button has a built-in 2fa/2sv key (think YubiKey), but it's disabled by default. If you use Two-Factor authentication on GitHub, Gmail etc, you can turn it on by entering this command on the Crosh shell (Ctrl+Alt+T):

```
u2f_flags u2f
```

# Linux Setup (AKA Crostini)

## Enable Linux Apps

1. Open drawer in bottom right and click gear icon
1. Scroll down to find **Linux Apps** or **Linux (Beta)** and select **TURN ON**
1. After the install runs, you should have an application in ChromeOS called `Terminal`

**Update: the situation described in this paragraph appears to stem from a variety of known Chrome OS bugs, so might well be alleviated in future** Warning about Linux apps: it seems when the Pixelbook goes into certain modes of deep sleep, all crostini apps are terminated. I've found that this happens the most if you leave it open & not plugged in. If it's plugged in & open, the crostini apps seem to persist. Be aware of this in terms of work in progress & long-running processes (including cron).

## Initial update of container

```sh
sudo apt update && sudo apt dist-upgrade
```

Note: some guides out there suggest setting the root password, but **never** do that unless you absolutely know what you're doing. If you need to use a feature that requires a password for root, set it on the Crostini default user instead:

```sh
sudo passwd
```

## General kit

If you're used to having certain system command line tools handy, you might need to install them on debian. I do:

```
sudo apt install mtr dnsutils
```

These days I use mtr rather than ping & traceroute, but if you wan to go old school you can do:

```
sudo apt install dnsutils traceroute iputils-ping
```

You might also want some other useful tools. I do:

```
sudo apt install zip whois tree xclip xz-utils nano
```

## Generate new SSH Key

I [prefer to use an Ed25519 key](https://medium.com/risan/upgrade-your-ssh-key-to-ed25519-c6e8d60d3c54).

```sh
ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519 -C "my_name.crostini"
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_ed25519
cat .ssh/id_ed25519.pub
# Paste in GitHub/GitLab/etc.
```

In order to [make sure SSH agent starts automatically](https://wiki.archlinux.org/index.php/SSH_keys#SSH_agents), and that only one ssh-agent process runs at a time, add the following to `.bashrc`:

```sh
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent > ~/.ssh-agent.pid
fi
if [[ ! "$SSH_AUTH_SOCK" ]]; then
    eval "$(<~/.ssh-agent.pid)"
fi
```

I prefer to just after each restart enter:

```sh
ssh-add ~/.ssh/id_ed25519
```

TODO: SSH Config (assh?)
TODO: I've worked through this doc up to this point.

But there are tools I've used in Linux to automatically pop up a prompt if a key hasn't been registered with the agent.

## Python environment

I then follow [these Python dev setup steps for Linux](https://gist.github.com/uogbuji/435d86684894997dc3d8d46da7da1ea9)

## Backup

Warning: Until [this bug](https://bugs.chromium.org/p/chromium/issues/detail?id=858815#c18) is addressed Crostini containers only grow in disk space; they never shrink, even if you delete files within them.

The main approach to backup is to export the entire container:

* Enable the "Crostini Backup" Flag – chrome://flags/#crostini-backup, then choose Restart
* Open chrome://settings/crostini/exportImport and select "Backup" followed by "Save". Ideally save this on a external drive.

To restore:

* Open chrome://settings/crostini/exportImport and select "Restore". Select the export archive file. After it's done the exported version of Crostini should be in place.

## File Sharing

Now automatic between Chrome OS and Linux. You'll find Linux files/directories in the Files app, under "Linux Files".

In order to access Chrome OS files you must make the desired folder or removable device available to Linux by right clicking in the Files app and selecting the option. **Note: It seems you might have to reboot the device before this "Share with Linux" option appears.**

### Google Drive

Go to the files app and right click on Google Drive, then click "Share with Linux"

Files will be available under: /mnt/chomeos/Googledrive

### Slack

[Download the Linux package](https://slack.com/downloads/linux). Install with Linux.

If you just launch & click "Sign in" you'll eventually land at the error: "We’re sorry
It looks like you are on a device that we don’t yet support."

Instead just go to https://[your-channel].slack.com/ssb/signin_redirect/fallback, click "Copy sign-in key" then launch slack, which will automatically pick up the magic key from your clipboard. Repeat for any other accounts you have.

### Dropbox

Unfortunately Dropbox support for ChromeOS is poor. Main recommendation seems to be the third-party [File System for Dropbox](https://chrome.google.com/webstore/detail/file-system-for-dropbox/hlffpaajmfllggclnjppbblobdhokjhe?hl=en) but it doesn't support offline access. The official Dropbox Linux client ended support for all file systems except unencrypted ext4, which rules it out for crostini. Looking into [rclone](https://www.reddit.com/r/Crostini/comments/9g9d59/dropbox_is_dropping_support_for_all_linux/) and just using the Web client for now.

## Skype

I tried the Skype for Linux DEB, but the microphone didn't work on Pixel Slate, and I've heard screen sharing might not work. I might try the Android app, but for now I'm just switching as much as I can to Google Meet.

## Applications

Crostini (the Linux container on Chrome OS) understands Debian installation packages (`.deb`) - You can just download any installer and double-click it to install.

And, being debian you also have `apt`, though in this case you might need to set up the installed apps to be launched by icon by placing a desktop file in `/usr/local/share/applications/`. [Details here](https://chromium.googlesource.com/chromiumos/platform2/+/master/vm_tools/garcon/#installed-applications).

```sh
sudo mkdir -p /usr/local/share/applications
```

Some examples next

### Terminal alternatives

So many choices, and so manby combos of distinct and/or overlapping and/or complementary tools. I'm trying just Extraterm

#### Extraterm

```sh
mkdir ~/Downloads
cd ~/Downloads
#Update to latest release
wget https://github.com/sedwards2009/extraterm/releases/download/v0.42.2/extraterm-0.42.2-linux-x64.zip
wget https://github.com/sedwards2009/extraterm/releases/download/v0.42.2/extraterm-commands-0.42.2.zip
cd /opt
sudo unzip ~/Downloads/extraterm-0.42.2-linux-x64.zip
sudo unzip ~/Downloads/extraterm-commands-0.42.2.zip
sudo ln -s /opt/extraterm-0.42.2-linux-x64/extraterm /usr/local/bin/
sudo apt install libnss3
```

Edit (`sudo vi /usr/share/applications/extraterm.desktop`) to get the launcher in Chrome OS. Contents:

```
[Desktop Entry]
Name=Extraterm
Keywords=shell;prompt;command;commandline;cmd;
Version=0.42.2
Comment=The swiss army chainsaw of terminal emulators
Exec=/usr/local/bin/extraterm
Icon=/opt/extraterm-0.42.2-linux-x64/resources/app/extraterm/resources/logo/extraterm_small_logo_256x256.png
Type=Application
```

Add to `.bashrc` the Extraterm command for shell integration, e.g. add the line:

```sh
. /opt/extraterm-commands-0.42.2/setup_extraterm_bash.sh
```

Note: [You would think](http://mywiki.wooledge.org/DotFiles) this should be added to `.profile` but for some reason Extraterm under Crostini does not run a login shell (i.e. running `shopt login_shell` yields `off`).

#### Tilix

```sh
echo 'deb http://ftp.debian.org/debian stretch-backports main' | sudo tee /etc/apt/sources.list.d/stretch-backports.list
sudo apt update
sudo apt -y install tilix
sudo ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh
```

Add to path

```sh
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
    source /etc/profile.d/vte.sh
fi
```

#### Gnome Terminal

```sh
sudo apt install gnome-terminal
```

Also:

* Terminator
* mate-terminal
* combos of the above with tmux or byobu

### Text editor/IDE

I was using TextMate on Mac and Atom in my Linux Mint installs. I'm not much of a fan of IDEs, but VSCode seems really popular among Pixelbook users, so I'll give it a whirl.

#### Atom

[Download the .deb installer from atom.io](https://atom.io/download/deb) (or the [beta version of Atom](https://atom.io/download/deb?channel=beta) if you prefer). Click "Show in folder"  right click then "Install in Linux"

#### VSCode

Download via chrome and click through to the download location in Files app. Right click then "Install in Linux"

If you use VSCode for Python dev and want to use VSCode plugins you'll want to do:

```
sudo apt install python3-pip
```

# Supplementary info

## Entering non-ASCII characters

Coming from Mac OS's excellent setup for international character entry I have to say ChromeOS falls quite short. From the commonplace (SHIFT+OPTION+ - = em dash) to the less common (ALT+ e / i / SHIFT+ALT+ x = Igbo nasal i with rising tone) such keyboard shortcuts are nowhere to be found.

Best friend is probably the Unicode entry hotkey CTRL+SHIFT+ u. When you press this you get an underlined u and you can type in a 4 digit hex Unicode value to get the character. Then you just have to remember your faborites (e.g. 2014 = em dash) or print out a chart to have handy. [Here is a handy table](https://www.groovypost.com/howto/type-special-characters-chromebook-accents-symbols-em-dashes).

There is the US International Keyboard (add it from the Input Methods menu near the screen bottom right), but if you mix other languages with English, you'll be annoyed e.g. that a regular apostrophe is always an accent acute.

Tip from mannkeithc on Reddit:

*Flip your Pixelbook into tablet mode, and use the onscreen virtual keyboard. You can hold down a character key and then slide over to the respective international character you want—very similar to an iPad or Mac OS. Bring this same virtual keyboard onto the screen when you are in clamshell/laptop mode by first selecting "Handwriting input". This brings up a virtual input area. Select the keyboard symbol towards LH top of this window to get the virtual keyboard. You can then use the touch screen as in tablet mode to select the international characters you want.*

## Clipboard history

On MacOS and Linux I've become used to the convenience of clipboard managers which e.g. allow you to manage a history of cut & paste, which can be very handy especially when editing code. It seems [Clipboard History Pro](https://chrome.google.com/webstore/detail/clipboard-history-pro/ajiejmhbejpdgkkigpddefnjmgcbkenk?hl=en) is the favorite choice of ChromeOS power users, but it's not yet entirely growing on mw. The keyboard shortcut doesn't seem to work and some of the other config options seem flaky. One note: it only stores the clipboard in your local browser storage, unless you explicitly upload an item to cloud storage (a useful feature), but I still recommend you never copy sensitive data such as passwords with any such tool.

## Using a pen

Pixelbook and Pixel slate are of course touch and pen devices, but you probably don't need to buy Google's $100 pen. You can find the Lenovo Active Pen 1 for about $20 and I was able to buy the Lenovo Active Pen 2 for just over $30. Many reviews indicate that these work as well for all but the most advanced users.

## Launch a Chrome browser tab

You can use the standard Python mechanism

`python -m webbrowser -t http://www.google.com`

Or you can use [garcon](https://chromium.googlesource.com/chromiumos/platform2/+/master/vm_tools/garcon/):

`/usr/bin/garcon-url-handler https://www.google.com`

If you want to change the browser used, set the `BROWSER` environment variable.

## Install Fonts

Some fonts can be installed using apt, for example I use Inconsolata

```sh
sudo apt install fonts-inconsolata -y
sudo fc-cache -fv
```

If you just have the font file:

- Copy .ttf file to ~/.local/share/fonts

```sh
fc-cache -vf ~/.local/share/fonts
fc-list | grep [your font name] # To verify
```

## HiDPI & Sommelier

The Pixelbook has a high-density display (HiDPI, roughly equivalent to the "Retina display" term if you're comming from a Mac), but not all Linux apps are prepared to appear correctly in HiDPI displays. They can appear small or have regular size with tiny cursor...

The good news is that you can use ChromeOS's built-in tool `Sommelier` to customize zoom levels for linux applications.

See also [this reddit thread](https://www.reddit.com/r/Crostini/comments/8vhta1/fix_for_resolution/).


### Example Sommelier setup (with Visual Studio Code)
Many installed programs will generate a `.desktop` file in /usr/share/applications, it contains an app description as well as its icon and launch command.


#### Example: `/usr/share/applications/code.desktop`

Change `Exec` entries to specify Sommelier's DPI and zoom level:

```
[Desktop Entry]
Name=Visual Studio Code
Comment=Code Editing. Redefined.
GenericName=Text Editor
Exec=sommelier -X --scale=0.7 --dpi=160 /usr/share/code/code "--unity-launch %F"
Icon=code
Type=Application
StartupNotify=true
StartupWMClass=Code
Categories=Utility;TextEditor;Development;IDE;
MimeType=text/plain;inode/directory;
Actions=new-empty-window;
Keywords=vscode;

X-Desktop-File-Install-Version=0.23

[Desktop Action new-empty-window]
Name=New Empty Window
Exec=sommelier -X --scale=0.7 --dpi=160 /usr/share/code/code "--new-window %F"
Icon=code

```

Specifically for VSCode, I would also suggest adding these to your user settings

```
"window.titleBarStyle": "custom",
"window.zoomLevel": 2,
"editor.mouseWheelScrollSensitivity": 0.5
```

# Key resources

* [uogbuji's "Pixelbook Setup" gist](https://gist.github.com/uogbuji/714d06ad3a9e081b69337107a0beff18): starting-point of this document
* [denolfe's "Pixelbook Linux Setup" gist](https://gist.github.com/denolfe/e458c7c1ce47a4db63ff190665089e6d): the origin of the above gist, has some new changes
* [Pixelbook keyboard shortcuts](https://support.google.com/pixelbook/answer/7503852?hl=en)
* [r/Crostini subreddit Wiki](https://www.reddit.com/r/Crostini/wiki/index)--huge wealth of useful info
* If your world extends beyond English-only: [Choose the keyboard language & special characters on your Pixelbook](https://support.google.com/pixelbook/answer/9134758?hl=en)
